import { createAction, props } from '@ngrx/store';

export const Crear = createAction(
                                '[Todo] Crear todo',
                                props<{texto: string}>()
                                );