import { createReducer, on } from '@ngrx/store';
import {Crear} from './todo.actions';
import { Todo } from './models/todo.model';

export const estadoInicial: Todo[] = [];

const _todoReducer = createReducer(estadoInicial,
    on( Crear, (state, {texto}) => [...state]),
    );

export function todoReducer(state, action){
    return _todoReducer(state, action);
}